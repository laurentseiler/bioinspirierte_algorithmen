var fs = require('fs'),
    file = 'data/tsp225.tsp',
    s;


/******** SETTINGS *******************/

var kmax = 12,
    max_better_neighbourhood = 20,
    max_iterations = 20000,
    no_change_max = 1000,
    output_rhythm = 10;


/******** OBJECT DEFINITIONS *******************/

function node(id, x, y) {
  this.id = +id;
  this.x = +x;
  this.y = +y;
}


/******** VARIABLE NEIGHBOURHOOD SEARCH ALGORITHM *******************/

function vns(arr, callback) {
  var k = 1, i = 0;
  var neighbours = [];
  var current_route = previous_route = no_change = 0;

  function do_work() {
    k = 1;
    previous_route = current_route;
    current_route = route(arr);

    if (previous_route === current_route)
      no_change++;
    else
      no_change = 0;

    while (k <= kmax) {
      betterNeighbour = get_better_neighbourhood(arr, k);
      if (betterNeighbour === null) {
        k++;
      } else {
        k = 1;
        arr = betterNeighbour;
      }
    }

    i++;

    if (i % output_rhythm == 0) {
      update_sigma_graph(arr);
      report_state("h-right", "<b>Iteration:</b> " + i +
          " &nbsp; &nbsp; <b>Current best route:</b> " + (current_route | 0));
    }

    if (i < max_iterations && no_change < no_change_max)
      setTimeout(do_work, 0);
    else
      callback(arr, i);
  }

  do_work();
}

function gen_neighbour(arr) {
  var x = Math.floor(Math.random() * arr.length);
  var y = Math.floor(Math.random() * arr.length);
  if (x > y) {
    var tmp = x;
    x = y;
    y = tmp;
  }

  var first = arr.slice(0, x);
  var second = arr.slice(x, y).reverse();
  var third = arr.slice(y);

  return first.concat(second.concat(third));
}

function get_better_neighbourhood(arr, k) {
  var current_route = route(arr);

  for (var i = 0; i < max_better_neighbourhood; i++) {
    var perm = arr.slice();
    for (var j = 0; j < k; j++) {
      perm = gen_neighbour(perm);
    }
    var this_route = route(perm);
    if (this_route < current_route)
      return perm;
  }

  return null;
}


/******** HELPER FUNCTIONS *******************/

function permute(perm) {
  for (i in perm) {
    var target = Math.floor(Math.random() * perm.length);
    var tmp = perm[i];
    perm[i] = perm[target];
    perm[target] = tmp;
  }
}

function dist(n, m) {
  return Math.sqrt(Math.pow(n.x - m.x, 2) + Math.pow(n.y - m.y, 2));
}

function route(arr) {
  var res = 0;
  for (i in arr) {
    var next_i = (i == arr.length - 1) ? 0 : +i + 1;
    res += dist(arr[i], arr[next_i]);
  }
  return res;
}


/******** STARTING POINT OF APPLICATION *******************/

fs.readFile(file, 'utf8', function(err, data) {
  if (err) {
    return echo(err);
  }

  var perm = [];
  var lines = data.split('\n');
  for (line of lines) {
    var tokens = line.trim().split(/\s+/);
    if (tokens.length != 3 || isNaN(Number(tokens[0])))
      continue;
    perm.push(new node(tokens[0], tokens[1], tokens[2]));
  }

  var num_nodes = perm.length;
  var route_before_perm = route(perm) | 0;
  permute(perm);
  var route_after_perm = route(perm) | 0;

  report_state("h-left", "Got <b>" + num_nodes + "</b> nodes &nbsp; &nbsp; " +
      "<b>Length before permutation:</b> " + route_before_perm +
      " &nbsp; &nbsp; &nbsp; <b>Length after permutation:</b> " + route_after_perm);

  init_sigma_graph(perm);
  update_sigma_graph(perm);

  vns(perm, function(perm, iteration) {
    sigma_graph_done();
    report_state("h-right", "<b>Iteration:</b> " + iteration +
        " &nbsp; &nbsp; <b>Final solution:</b> " + (route(perm) | 0));
  });
});


/******** OUTPUT FUNCTIONS *******************/

function report_state(where, msg) {
  if (typeof document === 'undefined') {
    echo(msg); // to command line if running in CLI mode
  } else {
    document.getElementById(where).innerHTML = msg;
  }
}

function echo(str) {
  console.log(str);
}


/******** GUI-RELATED FUNCTIONS *******************/

function init_sigma_graph(arr) {
  if (typeof sigma === 'undefined')
    return;

  var g = {
    nodes : [],
    edges : [],
  };

  for (node of arr) {
    g.nodes.push({
      id: 'n' + node.id,
      label: 'Node ' + node.id,
      x: node.x,
      y: -node.y,
      size: 1,
      color: '#ec5148',
    });
  }

  s = new sigma({
    graph: g,
    container: 'container',
    type: 'webgl',
    settings: {
      labelThreshold: 12,
      maxNodeSize: 5,
    },
  });
}

function update_sigma_graph(arr) {
  if (typeof sigma === 'undefined')
    return;

  var curr_edges = s.graph.edges();
  if (curr_edges.length > 0) {
    for (edge of curr_edges)
      s.graph.dropEdge(edge.id);
  }

  for (i in arr) {
    var this_node = arr[i],
        next_node = (i == arr.length - 1) ? arr[0] : arr[+i + 1];

    s.graph.addEdge({
      id: 'e' + this_node.id,
      source: 'n' + this_node.id,
      target: 'n' + next_node.id,
      color: '#bbb',
    });
  }

  s.refresh();
}

function sigma_graph_done() {
  if (typeof sigma === 'undefined')
    return;

  var nodes = s.graph.nodes();
  nodes.map(function(n) { n.color = '#669922'; });
  s.refresh();
}

// Optimal solution: 3916
