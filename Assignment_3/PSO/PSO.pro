#-------------------------------------------------
#
# Project created by QtCreator 2016-01-23T16:24:13
#
#-------------------------------------------------

QT       += core gui

CONFIG	 += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PSO
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    zdt1.cpp \
    mypoint.cpp \
    archive.cpp \
    population.cpp \
    rndgen.cpp

HEADERS  += mainwindow.h \
    zdt1.h \
    mypoint.h \
    archive.h \
    population.h \
    rndgen.h

FORMS    += mainwindow.ui
