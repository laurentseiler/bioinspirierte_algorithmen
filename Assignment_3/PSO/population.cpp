#include "population.h"

Population::Population(Archive *a, RndGen *r, size_t size, qreal inertia, qreal cogAccel, qreal socialAccel, qreal bounce)
    : archive(a), r(r), inertia(inertia), cogAccel(cogAccel), socialAccel(socialAccel), bounce(bounce)
{
    population = new QList<MyPoint *>();

    tUpRate = 0;
    genRandomPopulation(size);
    updateArchive();
}

void Population::simStep(int t)
{
    updateVelocity(t);
    updatePosition();
    updateArchive();
}

void Population::updateVelocity(int t)
{
    for (auto i = population->begin(); i < population->end(); i++) {

        if (t >= tUpRate) {
            QVector<qreal> *tmp = new QVector<qreal>(*(archive->getTarget()->getSolution()));
            (*i)->setTarget(tmp);
            tUpRate = 0;
        }

        QVector<qreal> *res1 = scalarMult(inertia, (*i)->getVelocity());
        QVector<qreal> *res2 = scalarMult(cogAccel, vecSub((*i)->getSubTarget(), (*i)->getSolution()));
        QVector<qreal> *res3 = scalarMult(socialAccel, vecSub((*i)->getTarget(), (*i)->getSolution()));

        (*i)->setVelocity(vecAdd(res1, vecAdd(res2, res3)));

        int k = 0;
        for (auto j = (*i)->getVelocity()->begin(); j < (*i)->getVelocity()->end(); j++) {
            if ((*j) > 0.4) {
                (*j) = 0.4;
            }

            k++;
        }

        delete res1;
        delete res2;
        delete res3;
    }

    tUpRate++;
}

void Population::updatePosition()
{
    for (auto i = population->begin(); i < population->end(); i++) {

        QVector<qreal> *newSolution = vecAdd((*i)->getSolution(), (*i)->getVelocity());

        for (int j = 0; j < zdt.getNumOfVars(); j++) {
            if (newSolution->value(j) < zdt.getLowerLimit()->value(j)) {
                newSolution->replace(j, zdt.getLowerLimit()->value(j));

                (*i)->getVelocity()->replace(j, (*i)->getVelocity()->value(j) * bounce);

            } else if (newSolution->value(j) > zdt.getUpperLimit()->value(j)) {
                newSolution->replace(j, zdt.getUpperLimit()->value(j));

                (*i)->getVelocity()->replace(j, (*i)->getVelocity()->value(j) * bounce);
            }
        }

        (*i)->setSolution(newSolution);

        qreal oldX = (*i)->x();
        qreal oldY = (*i)->y();

        zdt.evaluate(*i);

        if ((*i)->x() < oldX && (*i)->y() < oldY) {
            (*i)->updatePBest();
        }
    }
}

void Population::updateArchive()
{
    for (auto i = population->begin(); i < population->end(); i++) {
        archive->add((*i));
    }
}

void Population::genRandomPopulation(size_t size)
{
    for (size_t i = 0; i < size; i++) {
        population->append(genRandomPoint(zdt.getLowerLimit(), zdt.getUpperLimit(), zdt.getNumOfVars()));
    }

}

MyPoint *Population::genRandomPoint(QVector<qreal> *ll, QVector<qreal> *ul, size_t dim)
{
    MyPoint *p = new MyPoint(0, 0);

    QVector<qreal> *solution = new QVector<qreal>();

    for (size_t i = 0; i < dim; i++) {
        solution->append(r->getRnd(ll->value(i), ul->value(i)));
    }

    p->setSolution(solution);
    p->updatePBest();
    zdt.evaluate(p);

    return p;
}

QVector<qreal> *Population::scalarMult(qreal s, QVector<qreal> *v)
{
    QVector<qreal> *res = new QVector<qreal>();

    for (auto i = v->begin(); i < v->end(); i++) {
        res->append((*i) * s);
    }

    return res;
}

QVector<qreal> *Population::vecAdd(QVector<qreal> *v1, QVector<qreal> *v2)
{
    if (v1->size() != v2->size()) {
        std::cout << "Vector addition failed due to unequal length of arguments." << std::endl;
        return nullptr;
    }

    QVector<qreal> *res = new QVector<qreal>();

    // Not nice use data ptr
    auto j = v2->begin();
    for (auto i = v1->begin(); i < v1->end(); i++) {
        res->append((*i) + (*j));
        j++;
    }

    return res;
}

// Redundant
QVector<qreal> *Population::vecSub(QVector<qreal> *v1, QVector<qreal> *v2)
{
    if (v1->size() != v2->size()) {
        std::cout << "Vector subtraction failed due to unequal length of arguments." << std::endl;
        return nullptr;
    }

    QVector<qreal> *res = new QVector<qreal>();

    // Not nice use data ptr
    auto j = v2->begin();
    for (auto i = v1->begin(); i < v1->end(); i++) {
        res->append((*i) - (*j));
        j++;
    }

    return res;
}
