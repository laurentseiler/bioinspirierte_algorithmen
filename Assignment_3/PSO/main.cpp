#include "mainwindow.h"
#include "zdt1.h"
#include "archive.h"
#include <QApplication>
#include <iostream>
#include "population.h"
#include "rndgen.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    RndGen *r = new RndGen();
    Archive *archive = new Archive(r, 100);
    Population *pop = new Population(archive, r, 1000, 0.4, 2, 2, -0.3);


    for (int i = 0; i < 1500; i++) {
        pop->simStep(100);
    }

    w.drawArchive(archive->getArchive());

    archive->printArch();

    return a.exec();
}
