#ifndef MYPOINT_H
#define MYPOINT_H

#include <QPointF>
#include <QVector>

class MyPoint : public QPointF
{
public:
    MyPoint(qreal x, qreal y, QVector<qreal> *solution = nullptr);
    MyPoint* operator =(MyPoint *p);
    bool operator <(MyPoint *p);
    qreal getCrowdDist();
    void setCrowdDist(qreal val);
    QVector<qreal> *getSolution();
    void setSolution(QVector<qreal> *s);
    void setPbest(QVector<qreal> *b);
    QVector<qreal> *getPBest();
    QVector<qreal> *getVelocity();
    void setVelocity(QVector<qreal> *v);
    void updatePBest();
    void setTarget(QVector<qreal> *t);
    QVector<qreal> *getTarget();
    QVector<qreal> *getSubTarget();



private:
    QVector<qreal> *solution;
    QVector<qreal> *velocity;
    QVector<qreal> *pbest;
    QVector<qreal> *target;
    qreal cDist;

};

#endif // MYPOINT_H
