#include "archive.h"

Archive::Archive(RndGen *r, int size)
    : r(r), size(size)
{
    archive = new QList<MyPoint *>();
    cDistSorted = false;
}

bool Archive::add(MyPoint *p)
{
    if (checkDominated(p))
        return false;

    //TODO: Optimize by inserting at position
    //      (-> no ordering, better calcCrowdDist performance)
    MyPoint *pNew = new MyPoint(0,0);
    pNew = p;

    delDominated(pNew);
    archive->append(pNew);
    calcCrowdDist();
    cleanArchive();
    cDistSorted = false;

    return true;
}

MyPoint *Archive::getTarget()
{
    if (!cDistSorted)
        sortCDist();

    //size_t index = 2;

    //size_t upper = archive->size() < 3 ? archive->size() : 3;
    size_t index = (int) r->getRnd(1, 2);//(1 - qSqrt(r->getRnd(0, 1))) * archive->size();

    //std::cout << "Index: " << index << std::endl;

    return archive->at(index);
}

bool Archive::checkDominated(MyPoint *p)
{
    for (auto i = archive->begin(); i != archive->end(); i++) {
        if ((*i)->x() <= p->x() && (*i)->y() <= p->y())
            return true;
    }

    return false;
}

void Archive::delDominated(MyPoint *p)
{
    if (p != nullptr) {

        QMutableListIterator<MyPoint *> iter(*archive);

        while (iter.hasNext()) {
            iter.next();
            if (iter.value()->x() >= p->x() && iter.value()->y() >= p->y()) {
                iter.remove();
            }
        }
    }
}

void Archive::cleanArchive()
{
    /*if (p != nullptr) {
        for (auto i = archive->begin(); i < archive->end(); i++) {
            if ((*i)->x() >= p->x() && (*i)->y() > p->y()) {
                i = archive->erase(i);
            }
        }
    }*/

    if (archive->size() > size) {
        removeWorst();
    }
}

void Archive::calcCrowdDist()
{
    if (archive->size() < 3)
        return;

    std::sort(archive->begin(), archive->end(), cmpPointX<MyPoint>);
    archive->first()->setCrowdDist(std::numeric_limits<qreal>::max());
    archive->last()->setCrowdDist(std::numeric_limits<qreal>::max());

    for (auto i = ++(archive->begin()); i < --(archive->end()); i++) {
        MyPoint *pre = *(--(i));
        MyPoint *suc = *(++(i));

        (*i)->setCrowdDist(qFabs(suc->x() - pre->x()) * qFabs(suc->y() - pre->y()));
    }

}

void Archive::removeWorst()
{
    qreal currWorst = archive->value(0)->getCrowdDist();
    auto j = archive->begin();

    for (auto i = ++(archive->begin()); i < archive->end(); i++) {
        qreal tmp = (*i)->getCrowdDist();
        if (tmp < currWorst) {
            currWorst = tmp;
            j += (i-j);
        }
    }

    //std::cout << "Deleting: x: " << (*j)->x() << "\t y: " << (*j)->y() << "\t d: " << (*j)->getCrowdDist() << std::endl;
    archive->erase(j);
}

void Archive::sortCDist()
{
    std::sort(archive->begin(), archive->end(), cmpCDist<MyPoint>);
    cDistSorted = true;
}

size_t Archive::getCurrSize()
{
    return archive->size();
}

void Archive::printArch()
{
    for (auto i = archive->begin(); i < archive->end(); i++) {
        std::cout << "x: " << (*i)->x() << "\t y: " << (*i)->y() << "\t d: " << (*i)->getCrowdDist() << std::endl;
    }
}

QList<MyPoint *> *Archive::getArchive()
{
    return archive;
}
