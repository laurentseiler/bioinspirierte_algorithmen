#ifndef POPULATION_H
#define POPULATION_H

#include "mypoint.h"
#include "zdt1.h"
#include "rndgen.h"
#include "archive.h"
#include <QList>
#include <QVector>

class Population
{
public:
    Population(Archive *a, RndGen *r, size_t size, qreal inertia, qreal cogAccel, qreal socialAccel, qreal bounce);
    void simStep(int t);
    void updateVelocity(int t);
    void updatePosition();
    void updateArchive();
    void genRandomPopulation(size_t size);

private:
    Archive *archive;
    RndGen *r;
    qreal inertia;
    qreal cogAccel;
    qreal socialAccel;
    qreal bounce;
    int tUpRate;
    ZDT1 zdt;
    QList<MyPoint *> *population;
    MyPoint *genRandomPoint(QVector<qreal> *ll, QVector<qreal> *ul, size_t dim);
    QVector<qreal> *scalarMult(qreal s, QVector<qreal> *v);
    QVector<qreal> *vecAdd(QVector<qreal> *v1, QVector<qreal> *v2);
    QVector<qreal> *vecSub(QVector<qreal> *v1, QVector<qreal> *v2);
};

#endif // POPULATION_H
