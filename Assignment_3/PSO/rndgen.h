#ifndef RNDGEN_H
#define RNDGEN_H

#include <QTime>

class RndGen
{
public:
    RndGen();
    qreal getRnd(int l, int h);
};

#endif // RNDGEN_H
