#ifndef ZDT1_H
#define ZDT1_H

#include "mypoint.h"
#include <QVector>
#include <iostream>
#include <QtCore/qmath.h>

class ZDT1
{
public:
    ZDT1(int numOfVars = 30);
    ~ZDT1();

    MyPoint *evaluate(QVector<qreal> *solution);
    void evaluate(MyPoint *p);

    QVector<qreal> *getUpperLimit();
    QVector<qreal> *getLowerLimit();

    int getNumOfVars();

private:
    int numberOfVariables;
    int numberOfObjectives;
    int numberOfConstraints;

    QVector<qreal> *upperLimit;
    QVector<qreal> *lowerLimit;

    qreal evalG(QVector<qreal> *solution);
    qreal evalH(qreal f, qreal g);

    qreal truncate(qreal val);
};

#endif // ZDT1_H
