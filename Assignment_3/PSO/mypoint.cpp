#include "mypoint.h"

MyPoint::MyPoint(qreal x, qreal y, QVector<qreal> *solution) : QPointF(x, y), solution(solution) {
    // TODO: Parameterize this
    velocity = new QVector<qreal>(30, 0);
    cDist = 0;
}


MyPoint* MyPoint::operator =(MyPoint* p1)
{
    this->setX(p1->x());
    this->setY(p1->y());

    this->solution = new QVector<qreal>(*(p1->solution));

    return this;
}

bool MyPoint::operator <(MyPoint *p)
{
   return this->x() < p->x();
}

qreal MyPoint::getCrowdDist()
{
    return cDist;
}

void MyPoint::setCrowdDist(qreal val)
{
    cDist = val;
}

QVector<qreal> *MyPoint::getSolution()
{
    return solution;
}

void MyPoint::setSolution(QVector<qreal> *s)
{
    if(solution != nullptr)
        delete solution;
    solution = s;
}

void MyPoint::setPbest(QVector<qreal> *b)
{
    if(pbest != nullptr)
        delete pbest;
    pbest = b;
}

QVector<qreal> *MyPoint::getPBest()
{
    return pbest;
}

QVector<qreal> *MyPoint::getVelocity()
{
    return velocity;
}

void MyPoint::setVelocity(QVector<qreal> *v)
{
    if(velocity != nullptr)
        delete velocity;
    velocity = v;
}

void MyPoint::updatePBest()
{
    pbest = new QVector<qreal>(*solution);
}

void MyPoint::setTarget(QVector<qreal> *t)
{
    target = t;
}

QVector<qreal> *MyPoint::getTarget()
{
    return target;
}

QVector<qreal> *MyPoint::getSubTarget()
{
    return pbest;
}
