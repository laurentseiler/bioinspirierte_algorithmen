#include "zdt1.h"

ZDT1::ZDT1(int numOfVars)
{
    numberOfVariables = numOfVars;
    numberOfObjectives = 2;
    numberOfConstraints = 0;

    upperLimit = new QVector<qreal>();
    lowerLimit = new QVector<qreal>();

    for (int i = 0; i < numberOfVariables; i++)
    {
        upperLimit->append(1.0);
        lowerLimit->append(0.0);
    }
}

ZDT1::~ZDT1()
{
    delete upperLimit;
    delete lowerLimit;
}

MyPoint *ZDT1::evaluate(QVector<qreal> *solution)
{
    if (solution == nullptr || solution->size() != numberOfVariables)
        std::cerr << "Bad solution given at evaluate function." << std::endl;

    qreal g = evalG(solution);
    qreal h = evalH(solution->value(0), g);

    MyPoint *p = new MyPoint(solution->value(0), (h * g), solution);

    return p;
}

void ZDT1::evaluate(MyPoint *p)
{
    qreal g = evalG(p->getSolution());
    qreal h = evalH(p->getSolution()->value(0), g);

    p->setX(truncate(p->getSolution()->value(0)));
    p->setY(truncate(h * g));

    // TEST
    //std::cout << "Calculated: " << p->getSolution()->value(0) << " " << p->x() << " " << p->y() << std::endl;
}

qreal ZDT1::evalG(QVector<qreal> *solution)
{
    qreal g = 0.0;
    qreal constant = (9.0 / (numberOfVariables - 1));

    for (auto i = solution->begin() + 1; i < solution->end(); i++) {
       g += *i;
    }

    g *= constant;
    g++;

    return g;
}

qreal ZDT1::evalH(qreal f, qreal g)
{
    return 1.0 - qSqrt(f/g);
}

qreal ZDT1::truncate(qreal val)
{
    return ceil(val * 100) / 100;
}

QVector<qreal> *ZDT1::getUpperLimit()
{
    return upperLimit;
}

QVector<qreal> *ZDT1::getLowerLimit()
{
    return lowerLimit;
}

int ZDT1::getNumOfVars()
{
    return numberOfVariables;
}
