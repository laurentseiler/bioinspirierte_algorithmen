#ifndef ARCHIVE_H
#define ARCHIVE_H

#include "mypoint.h"
#include "rndgen.h"
#include <QList>
#include <QtMath>
#include <QVector>
#include <ctime>
#include <iostream>
#include <QtMath>

class Archive
{
public:
    Archive(RndGen *r, int size);
    bool add(MyPoint *p);
    MyPoint *getTarget();
    size_t getCurrSize();
    void printArch();
    QList<MyPoint *> *getArchive();

private:
    RndGen *r;
    int size;
    bool cDistSorted;
    QList<MyPoint *> *archive;
    bool checkDominated(MyPoint *p);
    void delDominated(MyPoint *p);
    void cleanArchive();
    void calcCrowdDist();
    void removeWorst();
    void sortCDist();

};

template <typename T> bool cmpPointX(T *a, T *b)
{
   return a->x() < b->x();
}

template <typename T> bool cmpCDist(T *a, T *b)
{
   return a->getCrowdDist() > b->getCrowdDist();
}

#endif // ARCHIVE_H
