#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::drawArchive(QList<MyPoint *> *a)
{
    int scaleFactor = 180;
    scene->clear();
    ui->graphicsView->viewport()->update();

    QBrush eBrush(Qt::green);
    QBrush eBrushBlack(Qt::black);
    QBrush eBrushRed(Qt::red);
    QPen ePen(Qt::black);
    ePen.setWidth(1);

    for (auto i = a->begin(); i < a->end(); i++) {
         scene->addEllipse(((*i)->x() * scaleFactor), (*i)->y() * -scaleFactor, 5, 5, ePen, eBrush);
    }

    //scene->addRect(0,0,5,-scaleFactor,ePen,eBrushBlack);
    //scene->addRect(0,0,scaleFactor,5,ePen,eBrushBlack);
    scene->addEllipse(scaleFactor, 0, 5, 5, ePen, eBrushRed);
    scene->addEllipse(0, -scaleFactor, 5, 5, ePen, eBrushRed);
    scene->addEllipse(0, 0, 5, 5, ePen, eBrushRed);
}
