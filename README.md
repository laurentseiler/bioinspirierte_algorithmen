# Bio-inspired Algorithms


-------------------


## Assignment 1: TSP solver with Variable Neighbourhood Search

### Authors
- Michael Huber
- Michael Wolf
- Laurent Seiler

### Produced values

The best achieved value has been **4173**.

![](Assignment_1/best.png)

### How to run

This program has a graphical user interface made with _Electron_ . It is
written in JavaScript and uses _Node.js_, which needs to be installed in
order to execute the program.

#### Use the GUI

- Install dependencies: Type `npm install`
- Run the GUI: Type `npm start`

#### Use in command-line mode

- **Hint:** Does not require Electron to run
- Type `node tsp_solver.js`

### Algorithm description

The main structure can be seen in the following function:

```{.javascript}
function vns(arr, callback) {
  var k = 1, i = 0;
  var neighbours = [];
  var current_route = previous_route = no_change = 0;

  function do_work() {
    k = 1;
    previous_route = current_route;
    current_route = route(arr);

    if (previous_route === current_route)
      no_change++;
    else
      no_change = 0;

    while (k <= kmax) {
      betterNeighbour = get_better_neighbourhood(arr, k);
      if (betterNeighbour === null) {
        k++;
      } else {
        k = 1;
        arr = betterNeighbour;
      }
    }

    i++;

    if (i < max_iterations && no_change < no_change_max)
      setTimeout(do_work, 0);
    else
      callback(arr, i);
  }

  do_work();
}
```


-------------------


## Assignment 2: Sudoku solver using a Genetic Algorithm

### Authors
- Michael Huber
- Laurent Seiler

### Results

The solver regularly solves the simple sudoku. Unfortunately, we have
not been able to solve _any_ of the evaluation set sudokus, although the
solver gets often as close as `fitness = 2`.

![](Assignment_2/run.png)

### How to run

The program is written in _Node.js_, which needs to be installed.
It has no graphical user interface. Install the node dependencies with
`npm install` and then just type `node sudoku_solver.js my_sudoku_file.txt`
to run the solver.

### Algorithm description

This algorithm is a little more complex. At first, when the sudoku is loaded,
we try to fill some cases using simple logic (the yellow cells). Then the GA
itself is run. The relevant functions are all in `classes.js`. Note that only
feasible _blocks_ are produced. The GA tries to solve the sudoku such that
the _colums_ and _rows_ are also feasible. Moreover, we have a _Feasibility Map_
for the individual cells, which tells us if a certain number can be in that
cell, based on the fixed and logically obtained values.

The **core of the algorithm** in the _init_ function...

```{.javascript}
  // Create initial population from initial field
  for (var i = 0; i < this.pop_size; i++) {
    this.val[i] = new Sudoku(this.initial_field.val.slice());
    permute(this.val[i]);
  }

  // Sort by fitness
  this.val.sort(this.compare);

  while(this.generation < this.max_generation && this.best != 0) {
    this.evolve();
    live("Generation "+this.generation+": Best="+this.best+", Worst="+this.worst+", " +
         "No change since "+this.no_change_counter+" generations.");
  }
  echo();
  echo("Final solution with fitness " + this.val[0].calc_fitness_1() + ":");
  echo(this.val[0]);
```

...and the **evolve** function:

```{.javascript}
  // Assumption: population is sorted best-to-worst by fitness.

  // Remove 'selection rate' percentage of population
  var drop_point = (this.val.length * this.selection_rate) >> 0;

  if (this.no_change_counter * this.pop_size > 500000) {
    drop_point = 1;
    this.no_change_counter = 1;
  }

  this.val.splice(drop_point, this.val.length - drop_point);

  // Generate children until population size is reached again
  while (this.val.length < this.pop_size) {

    // Take two random sudokus
    var r1 = (Math.random() * drop_point) >> 0;
    var r2 = (Math.random() * drop_point) >> 0
    var random = [
      this.val[r1],
      this.val[r2],
    ];

    // Do crossover and get resulting child
    var child = this.crossover.apply(this, random);

    // Mutate the child by 'mutation_rate' probability with a
    // certain strength
    if (Math.random() <= this.mutation_rate) {
      var strength = (Math.random() * this.max_mutation) >> 0;
      child.mutate(strength);
    }

    // Add child to population
    this.val.push(child);
  }
```

We have 3 **fitness functions**:

```{.javascript}
Sudoku.prototype.calc_fitness_1 = function () {

  var measure_dups = function (arr) {
    var penalty = 0,
        checklist = [false, false, false, false, false, false, false, false, false];

    for (chrom of arr) {
      var pos = chrom.val - 1;
        if (checklist[pos])
          penalty++;
        checklist[pos] = true;
      }
      return penalty;
    }

  var res = 0;

  for (var i = 0; i < 9; i++) {
    res += measure_dups(this.getline(i)) +
           measure_dups(this.getcolumn(i));
    // Check for block not necessary
  }

  return res;
}
```

```{.javascript}
Sudoku.prototype.calc_fitness_2 = function () {
  var res = 0;

  // Check feasibility of values in cells
  res += Sudoku.fs.check(this.val);

  return res;
}
```

```{.javascript}
Sudoku.prototype.calc_fitness_3 = function() {
  var res = 0;

  var calc_penealty = function (arr) {
    var sum = 0;
    for (var chrom of arr) {
      sum += chrom.val;
    }
    sum -= 45;
    if(sum < 0)
      sum *= -1;

    return sum;
  }

  for (var i = 0; i < 9; i++) {
    res += calc_penealty(this.getline(i)) +
           calc_penealty(this.getcolumn(i));
  }

  return res;
}
```

The first one gives a penalty for double values per row and column.

The second one gives a penalty if there are impossible values in a cell using
the aforementioned _Feasibility Map_.

The third one adds all the values together per row and column and checks
"how far away" we are from the expected sum. This function yielded bad
results, however.

Tests have shown empirically that this might be the best combination:

```{.javascript}
Sudoku.prototype.calc_fitness = function () {
  this.current_fitness = this.calc_fitness_1() + this.calc_fitness_2();
}
```


-------------------


## Assignment 3: Particle Swarm Optimization

### Authors
- Michael Huber
- Laurent Seiler

### Results

The red dots denote the coordinates _0,1_, _0,0_ and _1,0_.

![](Assignment_3/PSO/plot.jpg)

### How to run

It is a Qt5-based C++11 project. You need the Qt5 libraries in order to compile.

### Algorithm description

The algorithm was designed according to the methods of the lecture. A peculiarity
is the selection of the leaders. The assignment of a particle to the leader happens
every **n** steps (parameter of simulate step method).

```{.cpp}
pop->simStep(10); // Update leader every 10 generations only.
```



-------------------


## Assignment 4: Ant Colony Optimization

### Authors
- Michael Huber
- Laurent Seiler

### Results

Compared to the VNS, the results are awesome. It constantly achieves **below 3900**
and has already found the probably optimal path of **3858** a couple of times.

![](Assignment_4/best.png)

### How to run

It is a Qt5-based C++11 program with OpenMP parallelization. You need the Qt5
libraries in order to compile, OpenMP support is recommended. Build with
`qmake && make`, and then run with `./Assignment_4 data/tsp225.tsp`.

### Algorithm description

The main structure can be found in **aco.cpp**.

```{.cpp}
while(!stopCondition()) {
    QVector<Solution> currentSolutions(ants.size());

    // Every ant constructs a solution
    #pragma omp parallel for
    for (int i = 0; i < ants.size(); i++) {
        Solution &s = currentSolutions[i];
        int startPoint = rd() % graph.getNumOfPoints();
        ants[i]->init(startPoint);
        ants[i]->constructSolution(s);
    }

    // Graph receives solutions and updates pheromone
    graph.updatePhero(currentSolutions);

    // Update GUI!
    QString status;
    QTextStream(&status) << "Generation: " << currentGeneration << "    "
                         << "Best solution: " << (int)graph.getCurrentShortestPath().getLength();
    emit updateGui(status);
}
```

The ant's code is in **ant.cpp**. It uses "non-blind" next point search by combining
the pheromone and distance to probabilistically select the next point. When a complete
path has been made, a _2-opt_ is run over the solution.

```{.cpp}
void Ant::constructSolution(Solution &s)
{
    // Get a "to-visit" list of nodes
    QList<int> idList = g.getPoints()->keys();
    QHash<int, bool> visitedPoints;
    for (int id : idList)
        visitedPoints.insert(id, false);

    // Find way through the graph
    int numOfPoints = idList.length();
    int curr = startPoint;
    s.setStart(startPoint);

    for (int step = 0; step < numOfPoints - 1; step ++) {
        visitedPoints[curr] = true;

        // Get pheromones for curr to other (only possible) points
        QList<QPair<int, qreal> > phero = g.getPheroValuesFor(curr, visitedPoints);
        g.mergePheroValuesWithDist(curr, phero, 0.1);

        // Probabilistically select next point using pheromone value
        int next = probabilisticSelect(phero);

        // Add this to our solution
        qreal stepLength = g.getDistance(curr, next);
        s.addStep(next, stepLength);

        // Ready for next iteration!
        curr = next;
    }

    // Return to start
    qreal stepLength = g.getDistance(curr, startPoint);
    s.backToStart(stepLength);

    // Try to untie crossings using 2-opt
    try2opt(s, true);
}
```
