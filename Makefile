all: readme

readme:
	pandoc -s -S -H bootstrap.css --highlight-style pygments \
		-o README.html README.md
