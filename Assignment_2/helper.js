var lb_convert = function (arr, num) {
  // line - block convert
  var blockjmp = 3 * ((num % 3) >> 0);
  var linejmp = 27 * ((num / 3) >> 0);
  return [0,1,2, 9,10,11, 18,19,20]
    .map(function (elem) {
      return elem + blockjmp + linejmp;
    })
    .map(function (elem) {
      return arr[elem];
    });
}

var cb_convert = function (arr, num) {
  // column - block convert
  var blockjmp = 9 * ((num / 3) >> 0);
  var coljmp = ((num % 3) >> 0);
  return [0,3,6, 27,30,33, 54,57,60]
    .map(function (elem) {
      return elem + blockjmp + coljmp;
    })
    .map(function (elem) {
      return arr[elem];
    });
}

var echo = function (msg, newline) {
  msg = (msg == null) ? "" : '' + msg;
  newline = (newline == null) ? true : false;

  if (newline)
    console.log(msg);
  else
    process.stdout.write(msg);
}

var live = function (msg) {
    process.stdout.write(msg + "\r");
}

var fail = function (msg) {
  echo(msg);
  process.exit(1);
}

var parse_field = function(data, s) {
  var lines = data
    .split("\n")
    .filter(function (element, index, array) {
      return element.length > 0;
    });

  if (lines.length != 9)
    fail("Invalid sudoku given (parsed " + lines.length + " lines)");

  var blocks = [].concat.apply([], lines
    .map(function (line, index, array) {
      return line
        .split("")
        .filter(function (element, index, array) {
          return /[\w.-]/.test(element);
        })
        .map(function (element, index, array) {
          return new Chrom(element).fix();
        })
    }));

  s.val = [];
  for (var i = 0; i < 9; i++) {
    s.val = s.val.concat(lb_convert(blocks, i));
  }
}

var permute_array = function (arr) {
  for (var i in arr) {
    var target = Math.floor(Math.random() * arr.length);
    var tmp = arr[i];
    arr[i] = arr[target];
    arr[target] = tmp;
  }
}

var init_chrom = function (s) {
  for (var i = 0; i < 9; i++) {
    var values = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    var block = s.getblock(i);

    for (var j = 8; j >= 0; j--) {
      if (block[j].fixed)
        values[block[j].val - 1] = 0;
    }

    for (var j = 8; j >= 0; j--) {
      if (values[j] == 0)
        values.splice(j, 1);
    }

    for (var j = 0; j < 9; j++) {
      if (!block[j].fixed)
        block[j].val = values.pop();
    }
  }
}

var permute_block = function (s, i) {
    var block = s.getblock(i);
    var original = block.slice();

    for (var j = 8; j >= 0; j--) {
      if (block[j].fixed)
        block.splice(j, 1);
    }

    permute_array(block);

    for (var j = 0; j < 9; j++) {
      if (original[j].fixed) {
        block.splice(j, 0, original[j]);
      }
    }

    for (var j = 0; j < 9; j++) {
      s.val[9 * i + j] = block[j];
    }
}

var permute = function (s) {
  for (var i = 0; i < 9; i++) {
  	permute_block(s, i);
  }
}

var swap_two = function (s, blocknum) {
  var block = s.getblock(blocknum);

  for (var i = block.length - 1; i >= 0; i--) {
    if (block[i].fixed)
      block.splice(i, 1);
    else
      block[i] = i;
  }

  var random = [
    block[(Math.random() * block.length) >> 0],
    block[(Math.random() * block.length) >> 0],
  ];

  var first = s.val[9 * blocknum + random[0]];
  var second = s.val[9 * blocknum + random[1]];
  s.val[9 * blocknum + random[0]] = second;
  s.val[9 * blocknum + random[1]] = first;
}

module.exports = {
  lb_convert: lb_convert,
  cb_convert: cb_convert,
  echo: echo,
  live: live,
  fail: fail,
  init_chrom: init_chrom,
  parse_field: parse_field,
  permute_block: permute_block,
  permute: permute,
  swap_two: swap_two,
}
