fs = require("fs");
colors = require("colors");
helper = require("./helper.js");

Population = require("./classes").Population;
Sudoku = require("./classes").Sudoku;
Chrom = require("./classes").Chrom;

var fail = helper.fail,
    echo = helper.echo;

var pop = new Population();

if (process.argv.length < 3)
  fail("Not enough arguments!");

var argv = {
  input_filename: process.argv[2],
  pop_size: process.argv[3],
  selection_rate: process.argv[4],
  max_mutation: process.argv[5],
  mutation_rate: process.argv[6],
  max_generation: process.argv[7],
}

fs.readFile(argv.input_filename, 'utf-8', function (err, data) {
  if (err)
    fail("Could not find file '" + input_filename + "'!");

  echo(colors.bold("Trying to solve " + argv.input_filename + " ...\n"));
  pop.init(data, argv);
});
