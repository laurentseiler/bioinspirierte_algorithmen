helper = require("./helper");

var echo = helper.echo,
    live = helper.live,
    lb_convert = helper.lb_convert,
    cb_convert = helper.cb_convert,
    parse_field = helper.parse_field,
    permute_block = helper.permute_block,
    permute = helper.permute,
    init_chrom = helper.init_chrom,
    swap_two = helper.swap_two;



var Population = function () {
  this.generation = 0;
  this.best = -1;
  this.worst = -1;
}

Population.prototype.init = function (data, argv) {
  var if_set = function (val, def_val) {
    return (val == null) ? def_val : val;
  }

  this.pop_size = if_set(argv.pop_size, 100);
  this.selection_rate = if_set(argv.selection_rate, 0.5);
  this.max_mutation = if_set(argv.max_mutation, 10);
  this.mutation_rate = if_set(argv.mutation_rate, 1);
  this.max_generation = if_set(argv.max_generation, 1000);
  this.no_change_counter = 0;
  this.val = [];

  echo("Population size: " + colors.cyan(this.pop_size));
  echo("Selection rate: " + colors.cyan(this.selection_rate));
  echo("Max mutation: " + colors.cyan(this.max_mutation));
  echo("Mutation rate: " + colors.cyan(this.mutation_rate));
  echo("Max generations: " + colors.cyan(this.max_generation));
  echo();

  this.initial_field = new Sudoku();
  parse_field(data, this.initial_field);
  init_chrom(this.initial_field);

  // Initialize feasibility map
  Sudoku.fs = new FeasibilityMap();
  Sudoku.fs.init(this.initial_field.val.slice());

  // Create initial population from initial field
  for (var i = 0; i < this.pop_size; i++) {
    this.val[i] = new Sudoku(this.initial_field.val.slice());
    permute(this.val[i]);
  }

  // Sort by fitness
  this.val.sort(this.compare);

  while(this.generation < this.max_generation && this.best != 0) {
    this.evolve();
    live("Generation "+this.generation+": Best="+this.best+", Worst="+this.worst+", " +
         "No change since "+this.no_change_counter+" generations.");
  }
  echo();
  echo("Final solution with fitness " + this.val[0].calc_fitness_1() + ":");
  echo(this.val[0]);

  process.exit(this.val[0].calc_fitness_1() == 0 ? 0 : 1);
}

Population.prototype.compare = function (a, b) {
  if (a.fitness() < b.fitness())
    return -1;
  if (a.fitness() > b.fitness())
    return 1;
  return 0;
}

Population.prototype.evolve = function (callback) {
  // Assumption: population is sorted best-to-worst by fitness.

  // Remove 'selection rate' percentage of population
  var drop_point = (this.val.length * this.selection_rate) >> 0;

  if (this.no_change_counter * this.pop_size > 500000) {
    drop_point = 1;
    this.no_change_counter = 1;
  }

  this.val.splice(drop_point, this.val.length - drop_point);

  // Generate children until population size is reached again
  while (this.val.length < this.pop_size) {

    // Take two random sudokus
    var r1 = (Math.random() * drop_point) >> 0;
    var r2 = (Math.random() * drop_point) >> 0
    var random = [
      this.val[r1],
      this.val[r2],
    ];

    // Do crossover and get resulting child
    var child = this.crossover.apply(this, random);

    // Mutate the child by 'mutation_rate' probability with a
    // certain strength
    if (Math.random() <= this.mutation_rate) {
      var strength = (Math.random() * this.max_mutation) >> 0;
      child.mutate(strength);
    }

    // Add child to population
    this.val.push(child);
  }

  // Sort the new population again
  for (var sudoku of this.val)
    sudoku.calc_fitness();
  this.val.sort(this.compare);

  // Some bookkeeping
  this.generation++;
  this.oldbest = this.best;
  this.best = this.val[0].fitness();
  this.worst = this.val[this.val.length - 1].fitness();

  // Check for changes
  if (this.oldbest == this.best)
    this.no_change_counter++
  else
    this.no_change_counter = 0;

  // Call callback if something is given
  if (callback != null)
    callback(this);
}

Population.prototype.crossover = function (a, b) {
  var x_point = 9 * ((Math.random() * 8 + 1) >> 0);

  var a_part = a.val.slice(0, x_point);
  var b_part = b.val.slice(x_point, b.val.length);

  return new Sudoku(a_part.concat(b_part));
}



var Sudoku = function (val) {
  this.val = val;
  this.current_fitness = 0;
}

Sudoku.fs = null;

Sudoku.prototype.getline = function (num) {
  return lb_convert(this.val, num);
};

Sudoku.prototype.getcolumn = function (num) {
  return cb_convert(this.val, num);
};

Sudoku.prototype.getblock = function (num) {
  return this.val.slice(num * 9, (num * 9) + 9);
}

Sudoku.prototype.toString = function () {
  var res = "";
  for (var i = 0; i < 9; i++) {
    var line = this.getline(i);
    for (var j = 0; j < 9; j++) {
      res += "" + line[j] + "  ";
    if ((j + 1) % 3 == 0)
      res += "  ";
    }
    res += "\n";
    if ((i + 1) % 3 == 0)
      res += "\n";
  }
  return res;
}

Sudoku.prototype.fitness = function () {
  return this.current_fitness;
}

Sudoku.prototype.calc_fitness_1 = function () {

  var measure_dups = function (arr) {
    var penalty = 0,
        checklist = [false, false, false, false, false, false, false, false, false];

    for (chrom of arr) {
      var pos = chrom.val - 1;
        if (checklist[pos])
          penalty++;
        checklist[pos] = true;
      }
      return penalty;
    }

  var res = 0;

  for (var i = 0; i < 9; i++) {
    res += measure_dups(this.getline(i)) +
           measure_dups(this.getcolumn(i));
    // Check for block not necessary
  }

  return res;
}

Sudoku.prototype.calc_fitness_2 = function () {
  var res = 0;

  // Check feasibility of values in cells
  res += Sudoku.fs.check(this.val);

  return res;
}

Sudoku.prototype.calc_fitness_3 = function() {
  var res = 0;

  var calc_penealty = function (arr) {
    var sum = 0;
    for (var chrom of arr) {
      sum += chrom.val;
    }
    sum -= 45;
    if(sum < 0)
      sum *= -1;

    return sum;
  }

  for (var i = 0; i < 9; i++) {
    res += calc_penealty(this.getline(i)) +
           calc_penealty(this.getcolumn(i));
  }

  return res;
}

Sudoku.prototype.calc_fitness = function () {
  this.current_fitness = this.calc_fitness_1() + this.calc_fitness_2();
}

Sudoku.prototype.mutate = function (times) {
  if (Math.random() > 0.5) {
   permute_block(this, (Math.random() * 9) >> 0);
   return;
  }
  times = (times == null) ? 1 : times;

  for (var i = 0; i < times; i++) {
    var blocknum = (Math.random() * 9) >> 0;
    swap_two(this, blocknum);
  }
}



var Chrom = function (val, fixed) {
  this.val = val >> 0;
  this.fixed = (this.val == 0 || fixed == null) ? false : true;
  this.found = false;
}

Chrom.prototype.fix = function() {
  this.fixed = (this.val != 0) ? true : false;
  return this;
}

Chrom.prototype.toString = function() {
  if (this.found)
    return colors.yellow.bold(this.val);
  if (this.fixed)
    return colors.cyan.bold(this.val);
  if (this.val === 0)
    return "_";
  return this.val;
}


var FeasibilityMap = function () {
  this.val = [];

  // Initialize map
  for (var i = 0; i < 81; i++) {
    this.val[i] = [1, 2, 3, 4, 5, 6, 7, 8, 9 ];
  }

}

FeasibilityMap.prototype.init = function (initial_arr) {
  var getline = function (arr, num) {
    return lb_convert(arr, num);
  }

  var getcolumn = function (arr, num) {
    return cb_convert(arr, num);
  }

  var getblock = function (arr, num) {
    return arr.slice(num * 9, (num * 9) + 9);
  }

  var getfixed = function (arr) {
    return arr
      .filter(function (elem) { return elem.fixed; })
      .map(function (elem) { return elem.val; });
  }

  var remove_fixed_elements = function (target, reference) {
    for (var j in target) {
      for (var k = target[j].length - 1; k >= 0; k--) {
        if (reference.indexOf(target[j][k]) != -1) {
          target[j].splice(target[j].indexOf(target[j][k]), 1);
        }
      }
    }
  }

  // Remove infeasible values
  for (var i = 0; i < 9; i++) {
    var this_line = getfixed(getline(initial_arr, i)),
        this_column = getfixed(getcolumn(initial_arr, i)),
        this_block = getfixed(getblock(initial_arr, i));

    var this_mapline = getline(this.val, i),
        this_mapcolumn = getcolumn(this.val, i),
        this_mapblock = getblock(this.val, i);

    remove_fixed_elements(this_mapline, this_line);
    remove_fixed_elements(this_mapcolumn, this_column);
    remove_fixed_elements(this_mapblock, this_block);
  }

  for (var i = 0; i < 81; i++) {
    if (initial_arr[i].fixed)
      this.val[i] = [0, initial_arr[i].val];
  }

  var change = false;

  for (var i in this.val) {
    if (this.val[i].length < 2) {
      var block_i = i - (i % 9) >> 0;
      while (initial_arr[block_i].val != this.val[i][0])
        block_i++;

      var tmp = initial_arr[block_i].val;
      initial_arr[block_i].val = initial_arr[i].val;
      initial_arr[i].val = tmp;

      initial_arr[i].fixed = true;
      initial_arr[i].found = true;

      change = true;
    }
  }

  if (change) {
    this.init(initial_arr);
  }
}

FeasibilityMap.prototype.check = function (chrom_arr) {
  var penalty = 0;

  for (var i in chrom_arr) {
    if (!chrom_arr[i].fixed && this.val[i].indexOf(chrom_arr[i].val) == -1)
      penalty++;
  }

  return penalty;
}



module.exports = {
  Population: Population,
  Sudoku: Sudoku,
  Chrom: Chrom
}
