#include "ant.h"

void Ant::init(int startPoint)
{
    this->startPoint = startPoint;
}

void Ant::constructSolution(Solution &s)
{
    // Get a "to-visit" list of nodes
    QList<int> idList = g.getPoints()->keys();
    QHash<int, bool> visitedPoints;
    for (int id : idList)
        visitedPoints.insert(id, false);

    // Find way through the graph
    int numOfPoints = idList.length();
    int curr = startPoint;
    s.setStart(startPoint);

    for (int step = 0; step < numOfPoints - 1; step ++) {
        visitedPoints[curr] = true;

        // Get pheromones for curr to other (only possible) points
        QList<QPair<int, qreal> > phero = g.getPheroValuesFor(curr, visitedPoints);
        g.mergePheroValuesWithDist(curr, phero, 0.1);

        // Probabilistically select next point using pheromone value
        int next = probabilisticSelect(phero);

        // Add this to our solution
        qreal stepLength = g.getDistance(curr, next);
        s.addStep(next, stepLength);

        // Ready for next iteration!
        curr = next;
    }

    // Return to start
    qreal stepLength = g.getDistance(curr, startPoint);
    s.backToStart(stepLength);

    // Try to untie crossings using 2-opt
    try2opt(s, true);
}

int Ant::probabilisticSelect(QList<QPair<int, qreal> > &phero)
{
    // Sum up pheromone values
    qreal totalPhero = 0;
    for (auto p : phero)
        totalPhero += p.second;

    // Avoid collapse of the universe
    if (totalPhero == 0)
        return phero[mt() % phero.size()].first;

    // Normalize
    qreal factor = 10000.0 / totalPhero;
    for (auto &p : phero) {
        p = qMakePair(p.first, p.second * factor);
    }

    // Now select the actual target
    qreal target = ((mt() * 1.0) / (mt.max())) * 10000;
    qreal sum = 0;
    int i = 0, candidate = 0;
    while (sum < target) {
        candidate = phero[i].first;
        sum += phero[i].second;
        i++;
    }

    return candidate;
}

int Ant::simpleRandomSelect(QList<QPair<int, qreal> > &phero)
{
    return phero[mt() % phero.size()].first;
}

void Ant::try2opt(Solution &s, bool aggressive = true)
{
    const QList<int> trail = s.getTrail();
    qreal currBestLen = s.getLength();
    int first = 0, second = 0;

    for (int i = 0; i < trail.size() - 3; i++) {
        for (int j = i + 3; j < trail.size(); j++) {
            qreal oldLen = g.getDistance(trail[i], trail[i + 1]) +
                         g.getDistance(trail[j], trail[j - 1]);
            qreal newLen = g.getDistance(trail[i], trail[j - 1]) +
                         g.getDistance(trail[j], trail[i + 1]);
            qreal candidateLen = s.getLength() - oldLen + newLen;
            if (candidateLen < currBestLen) {
                if (!aggressive) {
                    s.perform2optSwap(i, j, candidateLen);
                    return;
                } else {
                    first = i;
                    second = j;
                    currBestLen = candidateLen;
                }
            }
        }
    }

    if (first != 0 && second != 0)
        s.perform2optSwap(first, second, currBestLen);
}
