#ifndef ANT_H
#define ANT_H

#include <QHash>
#include <QList>
#include <QDebug>
#include "solution.h"
#include "graph.h"
#include <random>

class Ant
{
public:
    Ant(Graph &graph)
        : g(graph)
        , mt(rd())
    {

    }
    void init(int startPoint);
    void constructSolution(Solution &s);
private:
    int startPoint;
    Graph &g;
    std::random_device rd;
    std::mt19937 mt;
    int probabilisticSelect(QList<QPair<int, qreal> > &phero);
    int simpleRandomSelect(QList<QPair<int, qreal> > &phero);
    void try2opt(Solution &s, bool aggressive);
};

#endif // ANT_H
