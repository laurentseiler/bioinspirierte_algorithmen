#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QElapsedTimer>
#include <QLineEdit>
#include <QDebug>
#include "mypoint.h"
#include "mygraphicslineitem.h"
#include "graph.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setInputFile(QString fn) {
        filename = fn;
    }

protected:
    void showEvent(QShowEvent *ev);
    void closeEvent(QCloseEvent *ev);
    void resizeEvent(QResizeEvent *ev);

signals:
    void windowLoaded();
    void resetButtonPressed();

public slots:
    void updateGui(QString msg);

private slots:
    void initGraph();
    void runACO();
    void buttonRunToReset();
    void buttonResetToRun();
    void resetGraphAndACO();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QGraphicsItemGroup pointGroup, edgeGroup;
    QList<MyGraphicsLineItem *> edges;
    Graph *graph;
    QElapsedTimer refreshTimer;
    QValidator *intValidator, *qrealValidator;

    QString filename;
    int   numAnts;
    int   generations;
    qreal pheroIncreaseStep;
    qreal pheroDecreaseFactor;
    qreal pheroSmallest;
    qreal pheroLargest;
    qreal pheroInit;

    bool viewAlreadyFit = false;
    void addGraphItemsToScene();
    void getAndSetParams();
    void updateGraph();
    void fitView();
    void setInputValidators();
};

#endif // MAINWINDOW_H
