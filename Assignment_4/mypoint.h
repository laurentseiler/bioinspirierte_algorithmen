#ifndef MYPOINT_H
#define MYPOINT_H

#include <QGraphicsEllipseItem>
#include <QBrush>
#include <QPen>

class MyPoint : public QGraphicsEllipseItem
{
public:
    MyPoint(qreal x, qreal y)
        : x(x)
        , y(y)
        , width(3)
        , height(3)
        , brush(Qt::black)
        , pen(Qt::black)
    {
        this->setRect(x, y, width, height);
        this->setBrush(brush);
        this->setPen(pen);
    }

    qreal getX() const { return x; }
    qreal getY() const { return y; }

private:
    qreal x, y, width, height;
    QBrush brush;
    QPen pen;

};

#endif // MYPOINT_H
