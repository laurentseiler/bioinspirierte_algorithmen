#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aco.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(ui->gv);
    ui->gv->setScene(scene);
    ui->gv->translate(0.5, 0.5);

    qApp->setQuitOnLastWindowClosed(true);
    setInputValidators();

    connect(this, &MainWindow::windowLoaded, this, &MainWindow::initGraph);
    connect(ui->runButton, &QPushButton::clicked, this, &MainWindow::runACO, Qt::QueuedConnection);
    connect(ui->runButton, &QPushButton::clicked, this, &MainWindow::buttonRunToReset);
}

void MainWindow::updateGui(QString msg)
{
    ui->statusBar->showMessage(msg);

    if(refreshTimer.hasExpired(200)) {
        updateGraph();
        refreshTimer.restart();
    }

    qApp->processEvents();
}


void MainWindow::showEvent(QShowEvent *ev)
{
    QMainWindow::showEvent(ev);
    emit windowLoaded();
}

void MainWindow::closeEvent(QCloseEvent *ev)
{
    (void)ev;
    exit(EXIT_SUCCESS);
}

void MainWindow::resizeEvent(QResizeEvent *ev)
{
    (void)ev;
    fitView();
}

void MainWindow::addGraphItemsToScene()
{
    PointIdx *points = graph->getPoints();

    for (int i = 0; i < points->size(); i++) {
        pointGroup.addToGroup((*points)[i]);
        edges.append(new MyGraphicsLineItem());
        edgeGroup.addToGroup(edges.back());
    }

    scene->addItem(&edgeGroup);
    scene->addItem(&pointGroup);

    refreshTimer.start();
}

void MainWindow::getAndSetParams()
{
    numAnts = ui->numAnts->text().toInt();
    generations = ui->generations->text().toInt();
    pheroIncreaseStep = ui->pheroIncrease->text().toDouble();
    pheroDecreaseFactor = ui->pheroDecrease->text().toDouble();
    pheroSmallest = ui->pheroSmallest->text().toDouble();
    pheroLargest = ui->pheroLargest->text().toDouble();
    pheroInit = ui->pheroInit->text().toDouble();
}

void MainWindow::updateGraph()
{
    const QList<int> &trailIds = graph->getCurrentShortestPath().getTrail();

    MyPoint *curr = graph->getPoint(trailIds.last());
    MyPoint *next = graph->getPoint(trailIds[0]);
    edges[0]->set(curr, next);

    for (int i = 1; i < trailIds.size(); i++) {
        curr = next;
        next = graph->getPoint(trailIds[i]);
        edges[i]->set(curr, next);
    }
}

void MainWindow::fitView()
{
    scene->setSceneRect(pointGroup.boundingRect());
    ui->gv->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
    ui->gv->scale(0.95, 0.95);
}

void MainWindow::setInputValidators()
{
    intValidator = new QIntValidator(0, INT_MAX, this);
    qrealValidator = new QDoubleValidator(0, INT_MAX, 10, this);

    ui->numAnts->setValidator(intValidator);
    ui->generations->setValidator(intValidator);
    ui->pheroIncrease->setValidator(qrealValidator);
    ui->pheroDecrease->setValidator(qrealValidator);
    ui->pheroSmallest->setValidator(qrealValidator);
    ui->pheroLargest->setValidator(qrealValidator);
    ui->pheroInit->setValidator(qrealValidator);
}

void MainWindow::buttonRunToReset()
{
    ui->runButton->setText("Reset");
    disconnect(ui->runButton, &QPushButton::clicked, this, &MainWindow::buttonRunToReset);
    disconnect(ui->runButton, &QPushButton::clicked, this, &MainWindow::runACO);
    connect(ui->runButton, &QPushButton::clicked, this, &MainWindow::resetGraphAndACO);
}

void MainWindow::buttonResetToRun()
{
    ui->runButton->setText("Run");
    disconnect(ui->runButton, &QPushButton::clicked, this, &MainWindow::resetGraphAndACO);
    connect(ui->runButton, &QPushButton::clicked, this, &MainWindow::runACO, Qt::QueuedConnection);
    connect(ui->runButton, &QPushButton::clicked, this, &MainWindow::buttonRunToReset);
}

void MainWindow::initGraph()
{
    graph = new Graph(filename);
    addGraphItemsToScene();
}

void MainWindow::runACO()
{
    getAndSetParams();
    graph->setSettings(pheroIncreaseStep, pheroDecreaseFactor, pheroSmallest, pheroLargest, pheroInit);

    std::cout << generations << " generations, " << numAnts << " ants. Pheromone inc +"
              << pheroIncreaseStep << ", dec (1 - " << pheroDecreaseFactor << "), smallest "
              << pheroSmallest << ", largest " << pheroLargest << ", init " << pheroInit
              << "." << std::endl;

    ACO aco(numAnts, generations, *graph, this);
    aco.run();

    std::cout << "Finished after " << aco.getCurrentGeneration() - 1 << " generations. Shortest path: "
              << (int)graph->getCurrentShortestPath().getLength() << "." << std::endl << std::endl;

    graph->reset();
    buttonResetToRun();
}

void MainWindow::resetGraphAndACO()
{
    emit resetButtonPressed();
    buttonResetToRun();
}

MainWindow::~MainWindow()
{
    delete graph;
    delete ui;
}
