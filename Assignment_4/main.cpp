#include "mainwindow.h"
#include <QApplication>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString filename("../data/tsp225.tsp");
    switch (argc) {
    case 2: filename = argv[--argc];
    }

    MainWindow w;
    w.setInputFile(filename);
    w.show();

    a.exec();
}
