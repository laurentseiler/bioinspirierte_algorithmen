#include "graph.h"

#include <QFile>
#include <QTextStream>
#include <iostream>

void Graph::readFromFile(QString filename)
{
    QFile inputFile(filename);

    if (inputFile.open(QIODevice::ReadOnly)) {
        QTextStream in(&inputFile);

        while (!in.atEnd()) {
            QString line = in.readLine();
            if (line.startsWith("NODE_COORD_SECTION"))
                break;
        }

        while (!in.atEnd()) {
            QString line = in.readLine();
            QStringList tmp = line.split(" ", QString::SkipEmptyParts);

            if (line == "EOF")
                continue;
            else if (tmp.size() != 3) {
                std::cerr << "Warning: Found bad coordinates: "
                          << line.toStdString() << " ...skipping" << std::endl;
                continue;
            }

            bool ok;

            int id = tmp.at(0).toInt(&ok) - 1;
            if (!ok) {
                std::cerr << "Found a bad id at line: "
                          << line.toStdString() << " ...skipping" << std::endl;
                continue;
            }

            qreal x = tmp.at(1).toDouble(&ok);
            if (!ok) {
                std::cerr << "Found a bad x coordinate at line: "
                          << line.toStdString() << " ...skipping" << std::endl;
                continue;
            }

            qreal y = tmp.at(2).toDouble(&ok) * -1;
            if (!ok) {
                std::cerr << "Found a bad y coordinate at line: "
                          << line.toStdString() << " ...skipping" << std::endl;
                continue;
            }

            MyPoint *p = new MyPoint(x, y);
            points->insert(id, p);
        }
    } else {
        std::cerr << "ERROR: Could not open file: "
                  << filename.toStdString() << std::endl;
        exit(EXIT_FAILURE);
    }

    inputFile.close();

    std::cout << "Added " << points->size() << " points from "
              << filename.toStdString() << "." << std::endl;
}

void Graph::initPheroMatrix(qreal initialValue)
{
    int len = points->size();

    pheros = new QVector<QVector<qreal> * >(len);

    for (int i = 0; i < len; i++) {
        auto *vec = new QVector<qreal>(len, initialValue);
        (*pheros)[i] = vec;
    }
}

void Graph::initDistMatrix()
{
    int len = points->size();

    distances = new QVector<QVector<qreal> * >(len);

    for (int i = 0; i < len; i++) {
        auto *col = new QVector<qreal>(len, 0);
        (*distances)[i] = col;
        for (int j = i + 1; j < len; j++) {
            (*col)[j] = calculateDistance(i, j);
        }
    }
}

void Graph::updatePhero(QVector<Solution> &solutions)
{
    // Solutions are sorted according to their length ( = quality)
    std::sort(solutions.begin(), solutions.end());

    // Add pheromone to trails
    for (int i = 0; i < solutions.size(); i++) {
        const QList<int> &trail = solutions[i].getTrail();
        qreal trailDistance = solutions[i].getLength();

        if (solutions[i] < currentShortestPath)
            currentShortestPath = solutions[i];

        for (int j = 0; j < trail.size(); j++) {
            int curr = trail[j];
            int next;
            if (j + 1 < trail.size())
                next = trail[j + 1];
            else
                next = trail[0];
            if (i < 3)
                increasePheroValue(curr, next, pheroIncreaseStep);
            else
                increasePheroValue(curr, next, pheroIncreaseStep / trailDistance);
        }
    }

    // Evaporate pheromone
    int len = pheros->size();
    #pragma omp parallel for
    for (int i = 0; i < len; i++) {
        for (int j = i + 1; j < len; j++) {
            qreal &phero = (*(*pheros)[i])[j];
            phero *= (1 - pheroDecreaseFactor);
            if (phero < pheroSmallest)
                phero = pheroSmallest;
        }
    }
}

void Graph::printPheroMatrix()
{
    int len = pheros->size();
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len; j++) {
            qreal &phero = (*(*pheros)[i])[j];
            printf("%6.4f  ", phero);
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

qreal Graph::getPheroValue(int from, int to)
{
    if (to == from)
        return -1;
    if (from > to) {
        int tmp = from;
        from = to;
        to = tmp;
    }
    return pheros->at(from)->value(to);
}

void Graph::increasePheroValue(int from, int to, qreal value)
{
    Q_ASSERT(from != to);
    if (to == from)
        exit(EXIT_FAILURE);
    if (from > to) {
        int tmp = from;
        from = to;
        to = tmp;
    }
    qreal &phero = (*(*pheros)[from])[to];
    phero += value;
    if (phero > pheroLargest)
        phero = pheroLargest;
}

QList<QPair<int, qreal> > Graph::getPheroValuesFor(int fromId, const QHash<int, bool> &visitedPoints)
{
    QList<int> allIds = points->keys();
    QList<QPair<int, qreal> > values;
    for (int toId : allIds) {
        if (fromId == toId)
            continue;
        if (!visitedPoints[toId])
            values.append(qMakePair(toId, getPheroValue(fromId, toId)));
    }
    Q_ASSERT(values.size() != 0);
    return values;
}

void Graph::mergePheroValuesWithDist(int fromId, QList<QPair<int, qreal> > &list, qreal distInfluence)
{
    qreal base = (pheroLargest - pheroSmallest) / 2;

    for (QPair<int, qreal> &city : list) {
        qreal dist = getDistance(fromId, city.first);
        qreal phero = city.second;
        qreal combined = (base * phero / dist) / (distInfluence * dist * dist * dist + 1);
        // combined = (1 + phero * (1 - distInfluence)) / ((1 + distInfluence * dist * dist) / base);
        city = qMakePair(city.first, combined);
    }
}

qreal Graph::getDistance(int fromId, int toId)
{
    if (toId == fromId)
        return 0;
    if (fromId > toId) {
        int tmp = fromId;
        fromId = toId;
        toId = tmp;
    }
    return distances->at(fromId)->at(toId);
}

qreal Graph::calculateDistance(int fromId, int toId)
{
    MyPoint *from = (*points)[fromId];
    MyPoint *to = (*points)[toId];

    qreal a = from->getX() - to->getX();
    qreal b = from->getY() - to->getY();

    return sqrt(a*a + b*b);
}

Graph::~Graph()
{
    for (auto arr : *pheros)
        delete arr;
    delete pheros;
    for (auto arr : *distances)
        delete arr;
    delete distances;
    delete points;
}
