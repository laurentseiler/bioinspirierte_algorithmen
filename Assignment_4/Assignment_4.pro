#-------------------------------------------------
#
# Project created by QtCreator 2016-01-22T12:19:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Assignment_4
TEMPLATE = app
CONFIG += c++11
QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp


SOURCES += main.cpp\
    mainwindow.cpp \
    graph.cpp \
    aco.cpp \
    ant.cpp

HEADERS  += mainwindow.h \
    mygraphicslineitem.h \
    graph.h \
    mypoint.h \
    aco.h \
    ant.h \
    solution.h

FORMS    += mainwindow.ui
