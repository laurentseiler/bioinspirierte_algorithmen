#ifndef SOLUTION_H
#define SOLUTION_H

#include <QList>
#include "mypoint.h"

class Solution
{
public:
    Solution(qreal length = 0) : length(length) { }
    bool operator <(const Solution& other) const {
        return length < other.getLength();
    }
    qreal getLength() const { return length; }
    const QList<int> &getTrail() const { return trail; }
    void setStart(int id) { trail.append(id); }
    void addStep(int id, qreal stepLength) {
        trail.append(id);
        length += stepLength;
    }
    void backToStart(qreal stepLength) { length += stepLength; }
    void reset(qreal length = 0) { this->length = length; }
    void perform2optSwap(int i, int j, qreal newLen) {
        int tmp;
        while (++i < --j) {
            tmp = trail[i];
            trail[i] = trail[j];
            trail[j] = tmp;
        }
        length = newLen;
    }

private:
    qreal length = 0;
    QList<int> trail;
};

#endif // SOLUTION_H
