#ifndef ACO_H
#define ACO_H

#include <QObject>
#include <QVector>
#include <QString>
#include <QTextStream>
#include "ant.h"
#include "graph.h"
#include "mainwindow.h"
#include <iostream>
#include <omp.h>
#include <random>

class ACO : public QObject
{
    Q_OBJECT
public:
    ACO(unsigned int maxGenerations, Graph &g, MainWindow *w)
        :ACO(omp_get_max_threads(), maxGenerations, g, w)
    {
    }
    ACO(int numOfAnts, unsigned int maxGenerations, Graph &g, MainWindow *w)
        : numOfAnts(numOfAnts)
        , maxGenerations(maxGenerations)
        , ants(QVector<Ant *>(numOfAnts))
        , graph(g)
        , gui(w)
    {
        init();
        connect(this, &ACO::updateGui, gui, &MainWindow::updateGui);
        connect(w, &MainWindow::resetButtonPressed, this, &ACO::abortACO);
    }
    ~ACO()
    {
        for (auto ant : ants)
            delete ant;
    }
    void run();
    unsigned int getCurrentGeneration() { return currentGeneration; }

signals:
    void updateGui(QString status);

public slots:
    void abortACO() { continueRunning = false; }

private:
    int numOfAnts;
    unsigned int currentGeneration = 0;
    unsigned int maxGenerations;
    bool continueRunning = true;
    QVector<Ant *> ants;
    Graph &graph;
    MainWindow *gui;
    std::random_device rd;
    void init();
    bool stopCondition();
};

#endif // ACO_H
