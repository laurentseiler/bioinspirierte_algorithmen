#ifndef MYGRAPHICSLINEITEM_H
#define MYGRAPHICSLINEITEM_H

#include <QGraphicsLineItem>
#include <QBrush>
#include <QPen>
#include "mypoint.h"

class MyGraphicsLineItem : public QGraphicsLineItem
{
public:
    MyGraphicsLineItem()
        : pen(Qt::darkCyan)
    {
        setLine(0,0,100,150);
        pen.setWidth(1);
        setPen(pen);
    }
    void set(const MyPoint *from, const MyPoint *to) {
        qreal x1, y1, x2, y2;
        x1 = from->getX() + 1.5;
        y1 = from->getY() + 1.5;
        x2 = to->getX() + 1.5;
        y2 = to->getY() + 1.5;
        setLine(x1, y1, x2, y2);
    }

private:
    QPen pen;
};

#endif // MYGRAPHICSLINEITEM_H
