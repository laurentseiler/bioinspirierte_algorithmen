#include "aco.h"

void ACO::init()
{
    // Initialize ant colony
    for (Ant *&ant : ants) {
        ant = new Ant(graph);
    }
}

void ACO::run()
{
    while(!stopCondition()) {
        QVector<Solution> currentSolutions(ants.size());

        // Every ant constructs a solution
        #pragma omp parallel for
        for (int i = 0; i < ants.size(); i++) {
            Solution &s = currentSolutions[i];
            int startPoint = rd() % graph.getNumOfPoints();
            ants[i]->init(startPoint);
            ants[i]->constructSolution(s);
        }

        // Graph receives solutions and updates pheromone
        graph.updatePhero(currentSolutions);

        // Update GUI!
        QString status;
        QTextStream(&status) << "Generation: " << currentGeneration << "    "
                             << "Best solution: " << (int)graph.getCurrentShortestPath().getLength();
        emit updateGui(status);
    }
}

bool ACO::stopCondition()
{
    return !continueRunning || ++currentGeneration > maxGenerations;
}

