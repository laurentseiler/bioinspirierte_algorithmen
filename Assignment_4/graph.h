#ifndef GRAPH_H
#define GRAPH_H

#include <QMap>
#include <QList>
#include <QDebug>
#include <QVector>
#include "mypoint.h"
#include "solution.h"

typedef QMap<int, MyPoint *> PointIdx;
typedef QVector<QVector<qreal> * > PheroMatrix;
typedef QVector<QVector<qreal> * > DistMatrix;

class Graph
{
public:
    Graph(QString filename)
        : currentShortestPath(INT_MAX)
    {
        points = new QMap<int, MyPoint *>();
        readFromFile(filename);
        initDistMatrix();
    }
    ~Graph();

    void setSettings(qreal pheroIncreaseStep, qreal pheroDecreaseFactor,
                qreal pheroSmallest, qreal pheroLargest, qreal pheroInit)
    {
        this->pheroIncreaseStep = pheroIncreaseStep;
        this->pheroDecreaseFactor = pheroDecreaseFactor;
        this->pheroSmallest = pheroSmallest;
        this->pheroLargest = pheroLargest;
        this->pheroInit = pheroInit;
        initPheroMatrix(pheroInit);
    }

    void reset() {
        for (auto arr : *pheros)
            delete arr;
        delete pheros;
        currentShortestPath.reset(INT_MAX);
    }

    void updatePhero(QVector<Solution> &solutions);
    void printPheroMatrix();

    PointIdx *getPoints() { return points; }
    MyPoint *getPoint(int id) { return (*points)[id]; }
    int getNumOfPoints() { return points->size(); }
    qreal getPheroValue(int from, int to);
    void increasePheroValue(int from, int to, qreal value);
    QList<QPair<int, qreal> > getPheroValuesFor(int id, const QHash<int, bool> &visitedPoints);
    void mergePheroValuesWithDist(int fromId, QList<QPair<int, qreal> > &list, qreal distInfluence);
    qreal getDistance(int fromId, int toId);
    qreal calculateDistance(int fromId, int toId);
    const Solution &getCurrentShortestPath() { return currentShortestPath; }

private:
    PointIdx *points;
    PheroMatrix *pheros;
    DistMatrix *distances;
    qreal pheroIncreaseStep = 0;        /* calculates: phero(p1, p2) += pIS / path_len */
    qreal pheroDecreaseFactor = 0.00;   /* calculates: phero(p1, p2) *= 1 - pDF */
    qreal pheroSmallest = 0.0001;
    qreal pheroLargest = 100000;
    qreal pheroInit = 20;
    Solution currentShortestPath;

    void readFromFile(QString filename);
    void initPheroMatrix(qreal initialValue);
    void initDistMatrix();

};

#endif // GRAPH_H
