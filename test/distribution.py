#!/usr/bin/python2

from random import random as rand
from math import sqrt

def random_select(arr):
  pos = int(rand() * len(arr))
  return pos

def biased_select_1(arr):
  pos = int(rand() * len(arr) * len(arr))
  bias = int(sqrt(pos))
  return bias

def biased_select_2(arr):
  idx = (int)((1 - sqrt(sqrt(rand()))) * len(arr))
  return idx

def biased_select_3(arr):
  idx = (int)((sqrt(rand())) * len(arr))
  return idx

def execute_test(msg, func):
  selected = []
  for i in xrange(m):
    selected.append(arr[func(arr)])

  print msg + ":"
  print_distribution(selected)

def print_distribution(arr):
  dist = {}
  for val in arr:
    if val not in dist:
      dist[val] = 1
    else:
      dist[val] += 1
  for k, v in dist.items():
    print str(k) + " occurs " + str(v) + " times"
  print

n = 10
m = 1000

arr = []
for i in xrange(1, n+1):
  arr.append(i)

execute_test("Random selection", random_select)
execute_test("Biased 1 selection", biased_select_1)
execute_test("Biased 2 selection", biased_select_2)
execute_test("Biased 3 selection", biased_select_3)
